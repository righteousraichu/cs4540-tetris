// TetrisGame.js
// CS4540 Final Project
//
// Created by Daniel James on 4/8/15.
// Copyright (c) 2015 Daniel James, John Robe, Riley Anderson. All rights reserved.

//
// TetrisGame is a generic model of a tetris game. This provides all the typical
// features such as hard drops, rotation, ghost piece, qeueu, etc...
//
var TetrisGame = function() {
    // The matrix contains every Block in the game.
    // Blocks in the top two rows should not be displayed.
    this.matrix = new Array(22);
    for (var i = 0; i < 22; i++) { // Initialize sub arrays
        this.matrix[i] = new Array(10);
    }

    // Contain Tetrimino objects
    this.falling = undefined;
    this.ghost = undefined;
    this.holdBlock = undefined;

    this.canHold = true;

    this.hasStarted = false;
    this.hasFinished = false;

    this.linesCleared = 0;
    this.score = 0;
    this.level = 1;

    this.remainingLinePoints = this.level * 5;

    this.backToBackTetris = false;


    /*
    * These anonymous functions are hooks that a game can use to be informed
    * of important events.
    *
    */
    this.gameFinishedHandler = function(){};
    this.cleanupBlocks = function(){}; // Called with blocks that should no longer be rendered.
    this.levelUp = function(){};

    // Only the first 5 elements in the queue should be displayed. (Next, plus the next 4)
    // The queue can be filled up 7 at a time. This is because each
    // tetrimino must appear once before repeats. (Tetris rules; AKA bag system)
    this.queue = [];

    // Start the game.
    this.startGame = function() {
        if (this.hasStarted) {
            console.log("Game has already been started, create a new instance for a new game.");
            return; // Already started
        }
        this.hasStarted = true;
        this.fillQueue();
        this.newFallingBlock();
    };

    // Should be called periodically. (The function that moves pieces down over time.)
    this.nextStep = function() {
        if (!this.hasStarted || this.hasFinished) {
            return;
        }

        if (!this.moveFalling(0, 1)) {
            if (this.falling.slideCounter-- === 0 && !(this.falling.slides++ < 10)) {
                this.canHold = true;
                this.newFallingBlock();
            }
        }
    };

    // Attempts to move the falling block.
    // If it is successfully moved, returns true.
    // This should be called when left/right have been pressed.
    this.moveFalling = function(dirX, dirY) {//(-1,0) - left, (1, 0) - right, (0, 1) down.
        if (!this.hasStarted || this.hasFinished) {
            return false;
        }

        var preX = this.falling.x;
        var preY = this.falling.y;

        var returnValue =  this.moveBlock(this.falling, dirX, dirY);
        this.setGhost();

        if (preX != this.falling.x || preY != this.falling.y) {
            console.log("resetting slide counter");
            this.falling.slideCounter = 1;
        }

        return returnValue;
    };

    // Switches out the current piece with the hold block.
    // Only allowed once per new block. (Otherwise you could keep switching blocks indefinitely.)
    this.hold = function() {
        // You can only hold once per new block.
        if (this.canHold) {
            this.canHold = false;
        } else {
            return;
        }

        // First time you hold there is no hold block!
        if (this.holdBlock !== undefined) {
            this.queue.unshift(this.holdBlock);
        }

        // Basically, we'll removed the blocks and readd them when neccessary.
        this.cleanupBlocks(this.falling.blocks);
        this.holdBlock = new Tetrimino(this.falling.type);

        for (var i = 0; i < this.falling.blocks.length; i++) {
            this.matrix[this.falling.blocks[i].y][this.falling.blocks[i].x] = undefined;
        }
        this.falling = undefined;

        // We put the hold block next in queue, so let this function do the rest.
        this.newFallingBlock();
    };

    // Should be called when "down" is pressed. (AKA speed up dropping but not instantly)
    this.softDrop = function() {
        var previousY = this.falling.y;
        this.moveFalling(0, 1);

        if (this.falling.y != previousY) {
            this.score++;//1 point for softDrop.
        }
    };

    // If this has been called, this REPLACES this.nextStep.
    this.hardDrop = function() {
        if (!this.hasStarted || this.hasFinished) {
            return;
        }

        var previousY = this.falling.y;

        //Move it all the way down.
        while(this.moveFalling(0, 1));

        var distanceMoved = this.falling.y - previousY;

        this.score += distanceMoved * 2; // 2 points per place moved.

        this.canHold = true;
        this.newFallingBlock();
    };

    // Rotate the piece. Pieces ALWAYS rotate, moving to where they fit.
    this.rotate = function(dir) { // 1 = right, -1 = left (technically you can double CW by 2... etc...)
        if (!this.hasStarted || this.hasFinished || this.falling.type == "O") { // Can't rotate O
        return;
    }

    this.falling.slideCounter = 1;

    this.removeBlocks = function(blocks) {
        // Remove from matrix.
        for (var i = 0; i < blocks.length; i++) {
            this.matrix[blocks[i].y][blocks[i].x] = undefined;
        }
    };

    this.fixBlocks = function(blocks) {
        var i, j, k;

        // Make sure they aren't hitting the wall.
        for (i = 0; i < blocks.length; i++) {
            if (blocks[i].x >= 10) { // Move the piece from the right wall.
                this.falling.x--;
                for (j = 0; j < blocks.length; j++) {
                    blocks[j].x--;
                    i = -1; // Reset loop.
                }
            } else
            if (blocks[i].x < 0) { // ... and from left.
                this.falling.x++;
                for (j = 0; j < blocks.length; j++) {
                    blocks[j].x++;
                    i = -1;
                }
            } else
            if (blocks[i].y >= 22) { // ... and from bottom.
                this.falling.y--;
                for (j = 0; j < blocks.length; j++) {
                    blocks[j].y--;
                    i = -1;
                }
            } else
            if (blocks[i].y < 0) { // ... and from top.
                this.falling.y++;
                for (j = 0; j < blocks.length; j++) {
                    blocks[j].y++;
                    i = -1;
                }
            }
        }

        // Float blocks up if they rotated into existing blocks.
        for (i = 0; i < blocks.length; i++) {
            if (this.matrix[blocks[i].y][blocks[i].x] !== undefined) {
                for (j = 0; j < blocks.length; j++) { // Float up.
                    blocks[j].y--;
                    i = -1; // Reset loop.
                }
            }
        }

        for (i = 0; i < blocks.length; i++) {
            this.matrix[blocks[i].y][blocks[i].x] = blocks[i];
        }

        this.setGhost();
    };

    this.falling.permutation += dir;

    while (this.falling.permutation < 0) {
        this.falling.permutation += 4;
    }

    this.removeBlocks(this.falling.blocks);
    this.falling.setPermutation();
    this.fixBlocks(this.falling.blocks);
};





/****** Internal Functions ******/




// Sets up a new falling block.
// Also checks for game over conditions.
this.newFallingBlock = function() {
    if (!this.hasStarted || this.hasFinished) {
        return;
    }

    var nextTetrimino = this.queue.shift();
    console.log("Next block up is: " + nextTetrimino.type);

    /** Check for game over conditions! **/
    if (this.falling !== undefined) {
        // Check to see if the game has been lost:
        // Game is over if last block landed above the matrix. (first two rows we shouldn't see.)
        this.hasFinished = true;
        for (var i = 0; i < this.falling.blocks.length; i++) {
            var block = this.falling.blocks[i];

            if (block.y > 1) {
                this.hasFinished = false;
            }
        }

        if (this.hasFinished) {
            this.gameFinishedHandler();
            return false;
        }
    }

    // Check if any rows should be cleared
    this.checkClearedLines();

    this.falling = nextTetrimino;
    for (i = 0; i < this.falling.blocks.length; i++) {
        var newBlock = this.falling.blocks[i];
        this.matrix[newBlock.y][newBlock.x] = newBlock;
    }
    this.setGhost();

    // Fill up queue if needed.
    if (this.queue.length < 5) {
        this.fillQueue();
    }

    return true;
};

// Checks for completed lines.
// Awards points as neccessary.
this.checkClearedLines = function() {
    var i, j;
    var rowsCleared = 0;
    for (i = 0; i < 22; i++) {
        var rowCleared = true;
        for (j = 0; j < 10; j++) {
            if (this.matrix[i][j] === undefined) {
                rowCleared = false;
                break;
            }
        }

        if (rowCleared) {
            // Row i cleared.
            console.log("Row " + i + " cleared.");
            rowsCleared++;

            this.cleanupBlocks(this.matrix[i]); // Callback to remove any additional data that might've been added to blocks. (ie Sprite)

            // Remove the blocks.
            this.matrix.splice(i,1);
            this.matrix.unshift(new Array(10));
            i--;
        }
    }

    if (rowsCleared > 0) {
        // Reset the matrix.
        for (i = 0; i < 22; i++) {
            for (j = 0; j < 10; j++) {
                if (this.matrix[i][j] !== undefined) {
                    this.matrix[i][j].x = j;
                    this.matrix[i][j].y = i;
                }
            }
        }

        // Increment score: http://tetris.wikia.com/wiki/Scoring
        var multiplier = 1;

        if (rowsCleared == 1) {
            multiplier = 100;
        } else if (rowsCleared == 2) {
            multiplier = 300;
        } else if (rowsCleared == 3) {
            multiplier = 500;
        } else if (rowsCleared == 4) {
            multiplier = 800;
        }

        if (rowsCleared == 4) {
            if (this.backToBackTetris) {
                multiplier = 1200;
                linePoints = 5;
                console.log("Back to Back Tetris");
            }
            this.backToBackTetris = true;
        } else {
            this.backToBackTetris = false;
        }

        this.score += multiplier * this.level;

        // Advance Levels
        var linePoints = multiplier/100;
        this.remainingLinePoints -= linePoints;
        if ((this.remainingLinePoints) <= 0) {
            //Level up!
            console.log("Level up!");
            this.level++;
            this.remainingLinePoints = this.level * 5;

            this.levelUp(); // Call level up handler.
        }
    }
};

// Moves a block. Checks to make sure the block is able to move.
// If the block was successfully moved, returns true.
this.moveBlock = function(blockToMove, dirX, dirY) {
    for (var i = 0; i < blockToMove.blocks.length; i++) {
        var block = blockToMove.blocks[i];
        if (block.x+dirX < 0 || block.x+dirX > 9 || block.y+dirY < 0 || block.y+dirY > 21) {
            return false;
        }

        // We need to make sure we're not checking if the tetrimino is running into itself.
        var shouldContinue = false;
        for (var j = 0; j < this.falling.blocks.length; j++) {
            if (this.falling.blocks[j] !== block && this.falling.blocks[j].x == block.x+dirX && this.falling.blocks[j].y == block.y+dirY) {
                // Just running into ourselves.
                shouldContinue = true;
            }
        }
        if (shouldContinue) {
            continue;
        }

        // Check if the tetrimino's blocks is running into a block in the matrix
        if (this.matrix[block.y+dirY][block.x+dirX] !== undefined) {
            // Occupied
            return false;
        }
    }


    // Actually move the tetrimino.
    blockToMove.x += dirX;
    blockToMove.y += dirY;
    if (blockToMove == this.falling) {
        for (i = 0; i < blockToMove.blocks.length; i++) {
            this.matrix[blockToMove.blocks[i].y][blockToMove.blocks[i].x] = undefined;
        }
    }
    for (i = 0; i < blockToMove.blocks.length; i++) {
        blockToMove.blocks[i].x += dirX;
        blockToMove.blocks[i].y += dirY;
        if (blockToMove == this.falling) {
            this.matrix[blockToMove.blocks[i].y][blockToMove.blocks[i].x] = blockToMove.blocks[i];
        }
    }


    return true;
}

// Adds 7 blocks to the queue
this.fillQueue = function() {
    var pos = this.queue.length; // position to start adding

    // Fill the bag with 0-6
    for (var i = 0; i < 7; i++) {
        this.queue.push(new Tetrimino(numberToType(i)));
    }

    // Shuffle the bag.
    for (i = pos; i < pos+7; i++) {
        var j = Math.floor((Math.random() * 7)+pos); // Random number between pos and pos+7
        var temp = this.queue[j];
        this.queue[j] = this.queue[i];
        this.queue[i] = temp;
    }
};

// Function to update the position of the ghost piece.
this.setGhost = function() {
    if (this.ghost === undefined) {
        this.ghost = new Tetrimino(this.falling.type);
    }

    // Make ghost match falling...
    this.ghost.x = this.falling.x;
    this.ghost.y = this.falling.y;
    for (var i = 0; i < this.ghost.blocks.length; i++) {
        this.ghost.blocks[i].x = this.falling.blocks[i].x;
        this.ghost.blocks[i].y = this.falling.blocks[i].y;
    }

    // Put ghost as low as possible.
    while (this.moveBlock(this.ghost, 0, 1));
}
};

// A Tetrimino is a group of 4 blocks.
// Each tetrimino contains an array of 4 Blocks.
// When a new tetrimino is created, the positions of its blocks are set to it's spawning position.
var Tetrimino = function(type) {
    this.blocks = [];
    this.type = type;
    this.slideCounter = 1;

    this.permutation = 0;
    this.x = 4;
    this.y = 1;

    if (type == "I") {
        this.blocks.push(new Block(3, 1, colorForType(type)));
        this.blocks.push(new Block(4, 1, colorForType(type)));
        this.blocks.push(new Block(5, 1, colorForType(type)));
        this.blocks.push(new Block(6, 1, colorForType(type)));
        this.y++;

        this.setPermutation = function() {
            if (this.permutation%4 === 0) {
                this.blocks[0].setPosition(this.x-1, this.y-1);
                this.blocks[1].setPosition(this.x, this.y-1);
                this.blocks[2].setPosition(this.x+1, this.y-1);
                this.blocks[3].setPosition(this.x+2, this.y-1);
            }
            if (this.permutation%4 === 1) {
                this.blocks[0].setPosition(this.x+1, this.y-2);
                this.blocks[1].setPosition(this.x+1, this.y-1);
                this.blocks[2].setPosition(this.x+1, this.y);
                this.blocks[3].setPosition(this.x+1, this.y+1);
            }
            if (this.permutation%4 === 2) {
                this.blocks[0].setPosition(this.x-1, this.y);
                this.blocks[1].setPosition(this.x, this.y);
                this.blocks[2].setPosition(this.x+1, this.y);
                this.blocks[3].setPosition(this.x+2, this.y);
            }
            if (this.permutation%4 === 3) {
                this.blocks[0].setPosition(this.x, this.y-2);
                this.blocks[1].setPosition(this.x, this.y-1);
                this.blocks[2].setPosition(this.x, this.y);
                this.blocks[3].setPosition(this.x, this.y+1);
            }
        };
    }
    if (type == "O") {
        this.blocks.push(new Block(4, 1, colorForType(type)));
        this.blocks.push(new Block(4, 0, colorForType(type)));
        this.blocks.push(new Block(5, 1, colorForType(type)));
        this.blocks.push(new Block(5, 0, colorForType(type)));

        this.setPermutation = function() {
        };
    }
    if (type == "T") {
        this.blocks.push(new Block(3, 1, colorForType(type)));
        this.blocks.push(new Block(4, 1, colorForType(type)));
        this.blocks.push(new Block(5, 1, colorForType(type)));
        this.blocks.push(new Block(4, 0, colorForType(type)));

        this.setPermutation = function() {
            if (this.permutation%4 === 0) {
                this.blocks[0].setPosition(this.x-1, this.y);
                this.blocks[1].setPosition(this.x, this.y);
                this.blocks[2].setPosition(this.x+1, this.y);
                this.blocks[3].setPosition(this.x, this.y-1);
            }
            if (this.permutation%4 === 1) {
                this.blocks[0].setPosition(this.x, this.y+1);
                this.blocks[1].setPosition(this.x, this.y);
                this.blocks[2].setPosition(this.x+1, this.y);
                this.blocks[3].setPosition(this.x, this.y-1);
                console.log(this);
                console.log(this.blocks);
            }
            if (this.permutation%4 === 2) {
                this.blocks[0].setPosition(this.x-1, this.y);
                this.blocks[1].setPosition(this.x, this.y);
                this.blocks[2].setPosition(this.x+1, this.y);
                this.blocks[3].setPosition(this.x, this.y+1);
            }
            if (this.permutation%4 === 3) {
                this.blocks[0].setPosition(this.x-1, this.y);
                this.blocks[1].setPosition(this.x, this.y);
                this.blocks[2].setPosition(this.x, this.y+1);
                this.blocks[3].setPosition(this.x, this.y-1);
            }
        };
    }
    if (type == "S") {
        this.blocks.push(new Block(3, 1, colorForType(type)));
        this.blocks.push(new Block(4, 1, colorForType(type)));
        this.blocks.push(new Block(4, 0, colorForType(type)));
        this.blocks.push(new Block(5, 0, colorForType(type)));

        this.setPermutation = function() {
            if (this.permutation%4 === 0) {
                this.blocks[0].setPosition(this.x-1, this.y);
                this.blocks[1].setPosition(this.x, this.y);
                this.blocks[2].setPosition(this.x, this.y-1);
                this.blocks[3].setPosition(this.x+1, this.y-1);
            }
            if (this.permutation%4 === 1) {
                this.blocks[0].setPosition(this.x, this.y-1);
                this.blocks[1].setPosition(this.x, this.y);
                this.blocks[2].setPosition(this.x+1, this.y);
                this.blocks[3].setPosition(this.x+1, this.y+1);
            }
            if (this.permutation%4 === 2) {
                this.blocks[0].setPosition(this.x-1, this.y+1);
                this.blocks[1].setPosition(this.x, this.y+1);
                this.blocks[2].setPosition(this.x, this.y);
                this.blocks[3].setPosition(this.x+1, this.y);
            }
            if (this.permutation%4 === 3) {
                this.blocks[0].setPosition(this.x-1, this.y-1);
                this.blocks[1].setPosition(this.x-1, this.y);
                this.blocks[2].setPosition(this.x, this.y);
                this.blocks[3].setPosition(this.x, this.y+1);
            }
        };
    }
    if (type == "Z") {
        this.blocks.push(new Block(3, 0, colorForType(type)));
        this.blocks.push(new Block(4, 0, colorForType(type)));
        this.blocks.push(new Block(4, 1, colorForType(type)));
        this.blocks.push(new Block(5, 1, colorForType(type)));

        this.setPermutation = function() {
            if (this.permutation%4 === 0) {
                this.blocks[0].setPosition(this.x-1, this.y-1);
                this.blocks[1].setPosition(this.x, this.y-1);
                this.blocks[2].setPosition(this.x, this.y);
                this.blocks[3].setPosition(this.x+1, this.y);
            }
            if (this.permutation%4 === 1) {
                this.blocks[0].setPosition(this.x+1, this.y-1);
                this.blocks[1].setPosition(this.x+1, this.y);
                this.blocks[2].setPosition(this.x, this.y);
                this.blocks[3].setPosition(this.x, this.y+1);
            }
            if (this.permutation%4 === 2) {
                this.blocks[0].setPosition(this.x-1, this.y);
                this.blocks[1].setPosition(this.x, this.y);
                this.blocks[2].setPosition(this.x, this.y+1);
                this.blocks[3].setPosition(this.x+1, this.y+1);
            }
            if (this.permutation%4 === 3) {
                this.blocks[0].setPosition(this.x, this.y-1);
                this.blocks[1].setPosition(this.x, this.y);
                this.blocks[2].setPosition(this.x-1, this.y);
                this.blocks[3].setPosition(this.x-1, this.y+1);
            }
        }
    }
    if (type == "J") {
        this.blocks.push(new Block(3, 0, colorForType(type)));
        this.blocks.push(new Block(3, 1, colorForType(type)));
        this.blocks.push(new Block(4, 1, colorForType(type)));
        this.blocks.push(new Block(5, 1, colorForType(type)));

        this.setPermutation = function() {
            if (this.permutation%4 === 0) {
                this.blocks[0].setPosition(this.x-1, this.y-1);
                this.blocks[1].setPosition(this.x-1, this.y);
                this.blocks[2].setPosition(this.x, this.y);
                this.blocks[3].setPosition(this.x+1, this.y);
            }
            if (this.permutation%4 === 1) {
                this.blocks[0].setPosition(this.x+1, this.y-1);
                this.blocks[1].setPosition(this.x, this.y-1);
                this.blocks[2].setPosition(this.x, this.y);
                this.blocks[3].setPosition(this.x, this.y+1);
            }
            if (this.permutation%4 === 2) {
                this.blocks[0].setPosition(this.x-1, this.y);
                this.blocks[1].setPosition(this.x, this.y);
                this.blocks[2].setPosition(this.x+1, this.y);
                this.blocks[3].setPosition(this.x+1, this.y+1);
            }
            if (this.permutation%4 === 3) {
                this.blocks[0].setPosition(this.x, this.y-1);
                this.blocks[1].setPosition(this.x, this.y);
                this.blocks[2].setPosition(this.x, this.y+1);
                this.blocks[3].setPosition(this.x-1, this.y+1);
            }
        };
    }
    if (type == "L") {
        this.blocks.push(new Block(3, 1, colorForType(type)));
        this.blocks.push(new Block(4, 1, colorForType(type)));
        this.blocks.push(new Block(5, 1, colorForType(type)));
        this.blocks.push(new Block(5, 0, colorForType(type)));

        this.setPermutation = function() {
            if (this.permutation%4 === 0) {
                this.blocks[0].setPosition(this.x-1, this.y);
                this.blocks[1].setPosition(this.x, this.y);
                this.blocks[2].setPosition(this.x+1, this.y);
                this.blocks[3].setPosition(this.x+1, this.y-1);
            }
            if (this.permutation%4 === 1) {
                this.blocks[0].setPosition(this.x, this.y-1);
                this.blocks[1].setPosition(this.x, this.y);
                this.blocks[2].setPosition(this.x, this.y+1);
                this.blocks[3].setPosition(this.x+1, this.y+1);
            }
            if (this.permutation%4 === 2) {
                this.blocks[0].setPosition(this.x-1, this.y+1);
                this.blocks[1].setPosition(this.x-1, this.y);
                this.blocks[2].setPosition(this.x, this.y);
                this.blocks[3].setPosition(this.x+1, this.y);
            }
            if (this.permutation%4 === 3) {
                this.blocks[0].setPosition(this.x-1, this.y-1);
                this.blocks[1].setPosition(this.x, this.y-1);
                this.blocks[2].setPosition(this.x, this.y);
                this.blocks[3].setPosition(this.x, this.y+1);
            }
        };
    }
};

// A very simple class that represents a block.
// It is expected that 'sprites' or other graphics be attached/linked with this object.
// And rendered as the block's x,y position changes.
var Block = function(x, y, color) {
    this.x = x;
    this.y = y;
    this.color = color;

    this.setPosition = function(x, y) {
        this.x = x;
        this.y = y;
    };
};

// Switch between letters and numbers.
// Does javascript have enums...?
// This is simply because it's easier to think in terms of letters than arbitary
// numbers.
function typeToNumber(type) {
    switch (type) {
        case "I" :
        return 0;
        case "O" :
        return 1;
        case "T" :
        return 2;
        case "S" :
        return 3;
        case "Z" :
        return 4;
        case "J" :
        return 5;
        case "L" :
        return 6;
        default:
        return 0;

    }
}

/// Switch between numbers and letters.
function numberToType(number) {
    switch (number) {
        case 0 :
        return "I";
        case 1 :
        return "O";
        case 2 :
        return "T";
        case 3 :
        return "S";
        case 4 :
        return "Z";
        case 5 :
        return "J";
        case 6 :
        return "L";
        default:
        return 0;

    }
}

// Helper function to easily get the color of a block.
function colorForType(type) {
    switch (type) {
        case "I" :
        return 0x3CC7ED;
        case "O" :
        return 0xFEFE03;
        case "T" :
        return 0xAB509B;
        case "S" :
        return 0x29FD2E;
        case "Z" :
        return 0xFC0D1B;
        case "J" :
        return 0x0B24FB;
        case "L" :
        return 0xED7930;
        default:
        return 0;

    }
}
