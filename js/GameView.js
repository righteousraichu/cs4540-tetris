// GameView.js
// CS4540 Final Project
//
// Copyright (c) 115 Daniel James, John Robe, Riley Anderson. All rights reserved
//
// The Main Menu
//
//

$(document).ready(function() {
  init();
});

function init() {
	var stage = new PIXI.Stage(0x22313f);
	// create a renderer instance
	var renderer = PIXI.autoDetectRenderer(window.innerWidth, window.innerHeight, null);
	renderer.view.style.height = '100%';
	renderer.view.style.width = '100%';
	// add the renderer view element to the DOM
	document.body.appendChild(renderer.view);
	loadMainMenu();
	requestAnimFrame(animate);

	// sound
	var sound = {
		//Music is copywrite free
	    background : new Audio("resources/tetris.mp3")
	};
	var muted = false;

	$( "#mute" ).click(function() {
	  toggleMute();
	});

	playBackgroundMusic();

	/**
	 * This function initializes the background music and plays it if it is not
	 * muted. Otherwise, the music is paused.
	 */
	function playBackgroundMusic() {
	    // check for setup
	    if (sound.background == undefined) {
	        return;
	    }

	    // setup sound.background
	    sound.background.loop = true;

	    // play or pause
	    if (muted) {
	        sound.background.pause();
	    }
	    else {
	        sound.background.play();
	    }
	}

	/**
	 * This function mutes the background music and stops the replay.
	 */
	function mute() {
	    muted = true;
	    $('#mute').html('Unmute').addClass('muted').removeClass('mute');
	    playBackgroundMusic();
	}

	/**
	 * This function unmutes the background music and restart the replay.
	 */
	function unmute() {
	    muted = false;
	    $('#mute').html('Mute').addClass('mute').removeClass('muted');
	    playBackgroundMusic();
	}

	/**
	 * This function toggles the background music and therefore mutes respectively
	 * unmutes it.
	 */
	function toggleMute() {
	    if (muted) {
	        unmute();
	    }
	    else {
	        mute();
	    }
	}

	function loadMainMenu()
    {
	    //Clear View
		for (var i = stage.children.length - 1; i >= 0; i--) {
			stage.removeChild(stage.children[i]);
		};
	    var isPortrait = window.innerHeight > window.innerWidth;
		var scene = new PIXI.DisplayObjectContainer();
		var gameScale;
		var gameWidth;
		var gameHeight;
		if (this.isPortrait) {
			// Portrait scaling
			gameWidth = 540;
			gameHeight = window.innerHeight * (540 / window.innerWidth);
			gameScale = window.innerWidth / 540;
		} else {
			// Landscape scaling
			gameWidth = window.innerWidth * (960 / window.innerHeight);
			gameHeight = 960;
			gameScale = window.innerHeight / 960;
		}

		scene.scale = new PIXI.Point(gameScale, gameScale);
		scene.x = window.innerWidth/2 - 540*gameScale/2;
	     // create a texture from an image path
		var image = PIXI.Texture.fromImage("resources/TETRIS.png");
		// create a new Sprite using the texture
		var menu = new PIXI.Sprite(image);
		// center the sprites anchor point
		menu.anchor.x = 0.5;
		menu.anchor.y = 0.5;
		// move the sprite to the center of the screen
		menu.position.x = 540 / 2;
		menu.position.y = 960 / 2;

		scene.addChild(menu);
		stage.addChild(scene);
		//Uses Jims Button... **Need to clean up**
		var start_button = new Button_Sprite(menu, "New Game", 200, 50, new_game, 0, true);
		start_button.x = 0;
		start_button.y = 50;

		var how_button = new Button_Sprite(menu, "How To Play", 200, 50, loadRules, 0, true);
		how_button.x = 0;
		how_button.y = 250;

    }

    function loadGameOver()
    {
	    //Clear View
		for (var i = stage.children.length - 1; i >= 0; i--) {
			stage.removeChild(stage.children[i]);
		};
	    var isPortrait = window.innerHeight > window.innerWidth;
		var scene = new PIXI.DisplayObjectContainer();
		var gameScale;
		var gameWidth;
		var gameHeight;
		if (this.isPortrait) {
			// Portrait scaling
			gameWidth = 540;
			gameHeight = window.innerHeight * (540 / window.innerWidth);
			gameScale = window.innerWidth / 540;
		} else {
			// Landscape scaling
			gameWidth = window.innerWidth * (960 / window.innerHeight);
			gameHeight = 960;
			gameScale = window.innerHeight / 960;
		}

		scene.scale = new PIXI.Point(gameScale, gameScale);
		scene.x = window.innerWidth/2 - 540*gameScale/2;
	     // create a texture from an image path
		var image = PIXI.Texture.fromImage("resources/TETRIS.png");
		// create a new Sprite using the texture
		var menu = new PIXI.Sprite(image);
		// center the sprites anchor point
		menu.anchor.x = 0.5;
		menu.anchor.y = 0.5;
		// move the sprite to the center of the screen
		menu.position.x = 540 / 2;
		menu.position.y = 960 / 2;

		var gameOverText = new PIXI.Text("Game Over...",{fill:'white', font:"bold 50px Arial"});
		gameOverText.anchor.x = 0.5;
		gameOverText.anchor.y = 0.5;

		gameOverText.position.x = 540 *.5;
		gameOverText.position.y = 960 *.4;

		scene.addChild(menu);
		scene.addChild(gameOverText);
		stage.addChild(scene);
		var start_button = new Button_Sprite(menu, "Back To Menu", 200, 50, backToMenu, 0, true);
		start_button.x = 0;
		start_button.y = 50;

    }

     function loadRules()
    {
	    //Clear View
		for (var i = stage.children.length - 1; i >= 0; i--) {
			stage.removeChild(stage.children[i]);
		};
	    var isPortrait = window.innerHeight > window.innerWidth;
		var scene = new PIXI.DisplayObjectContainer();
		var gameScale;
		var gameWidth;
		var gameHeight;
		if (this.isPortrait) {
			// Portrait scaling
			gameWidth = 540;
			gameHeight = window.innerHeight * (540 / window.innerWidth);
			gameScale = window.innerWidth / 540;
		} else {
			// Landscape scaling
			gameWidth = window.innerWidth * (960 / window.innerHeight);
			gameHeight = 960;
			gameScale = window.innerHeight / 960;
		}

		scene.scale = new PIXI.Point(gameScale, gameScale);
		scene.x = window.innerWidth/2 - 540*gameScale/2;
	     // create a texture from an image path
		var image = PIXI.Texture.fromImage("resources/TETRIS_Blank.png");
		// create a new Sprite using the texture
		var menu = new PIXI.Sprite(image);
		// center the sprites anchor point
		menu.anchor.x = 0.5;
		menu.anchor.y = 0.5;
		// move the sprite to the center of the screen
		menu.position.x = 540 / 2;
		menu.position.y = 960 / 2;

		//Add how to play text
		var gameOverText = new PIXI.Text("How To Play",{fill:'white', font:"bold 50px Arial"});
		gameOverText.anchor.x = 0.5;
		gameOverText.anchor.y = 0.5;

		gameOverText.position.x = 540 *.5;
		gameOverText.position.y = 960 *.05;

		//Keyboard text
		var KeyboardText = new PIXI.Text("Keyboard",{fill:'white', font:"bold 40px Arial"});
		KeyboardText.anchor.x = 0.5;
		KeyboardText.anchor.y = 0.5;

		KeyboardText.position.x = 540 *.5;
		KeyboardText.position.y = 960 *.15;

		//Keyboard controls
		var KeyboardKey = new PIXI.Text("Right Arrow (D) - Move Right\nLeft Arrow (A) - Move Left\nDown Arrow (S) - Move Down\nUp Arrow (W) - Rotate Clockwise\nZ - Rotate Counter-Clockwise\nSpacebar - Hard Drop\nShift - Hold\n",{fill:'white', font:"bold 30px Arial"});
		KeyboardKey.anchor.x = 0.5;
		KeyboardKey.anchor.y = 0.5;

		KeyboardKey.position.x = 540 * .45;
		KeyboardKey.position.y = 960 *.35;


		//Touch Text
		var TouchText = new PIXI.Text("Touch",{fill:'white', font:"bold 40px Arial"});
		TouchText.anchor.x = 0.5;
		TouchText.anchor.y = 0.5;

		TouchText.position.x = 540 *.5;
		TouchText.position.y = 960 *.50;

		//Touch Controlls
		var TouchKey = new PIXI.Text("Swipe Right - Move Right\nSwipe Left - Move Left\nSwipe Down - Move Down\nSwipe Up - Rotate Clockwise\nTap - Hard Drop\nPinch - Hold\n",{fill:'white', font:"bold 30px Arial"});
		TouchKey.anchor.x = 0.5;
		TouchKey.anchor.y = 0.5;

		TouchKey.position.x = 540 * .4;
		TouchKey.position.y = 960 *.70;

		scene.addChild(menu);
		scene.addChild(gameOverText);
		scene.addChild(KeyboardText);
		scene.addChild(KeyboardKey);
		scene.addChild(TouchText);
		scene.addChild(TouchKey);
		stage.addChild(scene);
		var start_button = new Button_Sprite(menu, "Back To Menu", 200, 50, backToMenu, 0, true);
		start_button.x = 0;
		start_button.y = 450;

    }

	function animate() {
		requestAnimFrame(animate);
		renderer.render(stage);
	}

	function new_game() {
		start_game();
	}

	function backToMenu() {
		loadMainMenu();
	}

	//Start the game. Update View
	function start_game() {

		var scene = new PIXI.DisplayObjectContainer();
		var gameScale;
		var gameWidth;
		var gameHeight;
		var nextTetrimino;

        var time = 750;

        var TimerFunction = function() {
            game.nextStep();
            clearInterval(interval);
            interval = setInterval(TimerFunction, time);
        }
        var interval = setInterval(TimerFunction, time);


		requestAnimFrame(animateGame);
		//Clear View
		for (var i = stage.children.length - 1; i >= 0; i--) {
			stage.removeChild(stage.children[i]);
		};
		var game = new TetrisGame();
		game.startGame();

		game.gameFinishedHandler = function(){
			loadGameOver();
		};

	
		game.cleanupBlocks = function(blocks) {
		    for (var i = 0; i < blocks.length; i++) {
		        if (blocks[i].sprite !== undefined) {
                    matrix.removeChild(blocks[i].sprite);
		        }
		    }
		};
		
		
        game.levelUp = function() {
            level.x = matrix.x + matrix.width - level.width;

            if (game.level >= 1 && game.level <= 5) {
                time -= 100;
            }
            if (game.level >= 7 && game.level <= 10) {
                time -= 50;
            }
            if (game.level >= 12 && game.level <= 15) {
                time -= 50;
            }
        };

		if (this.isPortrait) {
			// Portrait scaling
			gameWidth = 600;
			gameHeight = window.innerHeight * (gameWidth / window.innerWidth);
			gameScale = window.innerWidth / gameWidth;
		} else {
			// Landscape scaling
			gameHeight = 670;
			gameWidth = window.innerWidth * (gameHeight / window.innerHeight);
			gameScale = window.innerHeight / gameHeight;
		}
		scene.scale = new PIXI.Point(gameScale, gameScale);
		scene.x = window.innerWidth/2 - 300*gameScale/2;

		var blockSize = 30;

		var matrixGraphics = new PIXI.Graphics();
		matrixGraphics.lineStyle(1, 0xFFFFFF);
		for (i = 0; i < 22; i++) {
		    for (j = 0; j < 10; j++) {
		        matrixGraphics.drawRect(j*blockSize, i*blockSize, blockSize, blockSize);
		    }
		}
        matrixGraphics.lineStyle(2, 0xFFFFFF);
        matrixGraphics.drawRect(0, 0, blockSize*10, blockSize*22);
		var matrix = new PIXI.Sprite(matrixGraphics.generateTexture());

		var upcomingMatrixGraphics = new PIXI.Graphics();
		upcomingMatrixGraphics.lineStyle(2, 0xFFFFFF);
        upcomingMatrixGraphics.drawRect(0, 0, blockSize*4.5, blockSize*3);
        upcomingMatrixGraphics.drawRect(0, blockSize*4, blockSize*4.5, blockSize*3*4);
		var upcomingMatrix = new PIXI.Sprite(upcomingMatrixGraphics.generateTexture());

        upcomingMatrix.x = 350;
        upcomingMatrix.y = 60;

        var holdMatrixGraphics = new PIXI.Graphics();
        holdMatrixGraphics.lineStyle(2, 0xFFFFFF);
        holdMatrixGraphics.drawRect(0, 0, blockSize*4.5, blockSize*3);
		var holdMatrix = new PIXI.Sprite(holdMatrixGraphics.generateTexture());

        holdMatrix.x = 0-holdMatrix.width-blockSize;
        holdMatrix.y = 60;

        var matrixMask = new PIXI.Graphics();
        matrixMask.beginFill();
        matrixMask.drawRect(holdMatrix.x, blockSize*2-blockSize*.1, (upcomingMatrix.x+upcomingMatrix.width)-holdMatrix.x, matrix.height-blockSize*2+blockSize*.1);
    
        matrixMask.endFill();

        matrix.mask = matrixMask;

		// Center in Stage
		matrix.x = 0;
		matrix.y = 0;

		scene.addChild(matrix);
        scene.addChild(matrixMask);
		scene.addChild(upcomingMatrix);
        scene.addChild(holdMatrix);
		stage.addChild(scene);

		var blockTexture = PIXI.Texture.fromImage("resources/block.png");

        var scoreLabel = new PIXI.Text("Score: ", {font:"25px Arial", fill:"white"});
        var score = new PIXI.Text("0", {font:"25px Arial", fill:"white"});
        var level = new PIXI.Text("1", {font:"25px Arial", fill:"white"});
        scoreLabel.x = matrix.x;
        scoreLabel.y = matrix.y + blockSize;
        score.x = scoreLabel.x + scoreLabel.width;
        score.y = scoreLabel.y;

        level.x = matrix.x + matrix.width - level.width;
        level.y = scoreLabel.y;

        scene.addChild(scoreLabel);
        scene.addChild(score);
        scene.addChild(level);

		//Animation of the Blocks
		function animateGame() {
			var i, j, block;

			//Ghost
			for (i = 0; i < game.ghost.blocks.length; i++) {
				block = game.ghost.blocks[i];
				if (block.sprite === undefined) {
					block.sprite = new PIXI.Sprite(blockTexture);
					block.sprite.width = block.sprite.height = blockSize;
					block.sprite.alpha = 0.5;
                    matrix.addChild(block.sprite);
				}
				block.sprite.x = matrix.x+block.x*blockSize;
				block.sprite.y = matrix.y+block.y*blockSize;
			}

		    //Update with the current blocks
		    for (i = 0; i < game.matrix.length; i++) {
		        for (j = 0; j < game.matrix[i].length; j++) {
		            if (game.matrix[i][j] !== undefined) {
		                block = game.matrix[i][j];

                        attachSpriteToBlock(block);

		                block.sprite.x = matrix.x+block.x*blockSize+0.5;
		                block.sprite.y = matrix.y+block.y*blockSize+0.5;
		            }

		        }
		    }

		    //Update with the upcoming blocks
            for (i = 0; i < 5; i++) {
                var tetrimino = game.queue[i];
                for (j = 0; j < tetrimino.blocks.length; j++) {
				    block = tetrimino.blocks[j];
	                attachSpriteToBlock(block);

	                block.sprite.x = upcomingMatrix.x+block.x*blockSize - 3* blockSize + blockSize*0.25;
                    if (!(tetrimino.type == "O" || tetrimino.type == "I")) {
                        block.sprite.x += blockSize*0.5;
                    }

	                block.sprite.y = upcomingMatrix.y+block.y*blockSize  + blockSize + i*blockSize*3 - blockSize*0.5;
                    if (tetrimino.type == "I") {
                        block.sprite.y -= blockSize*0.5;
                    }
                    if (i > 0) {
                        block.sprite.y += blockSize;
                    }
	            }
            }

            // Update hold Blocks
            if (game.holdBlock !== undefined) {
                for (i = 0; i < game.holdBlock.blocks.length; i++) {
                    block = game.holdBlock.blocks[i];
	                attachSpriteToBlock(block);

                    block.sprite.x = holdMatrix.x+block.x*blockSize - 3* blockSize + blockSize*0.25;
                    if (!(game.holdBlock.type == "O" || game.holdBlock.type == "I")) {
                        block.sprite.x += blockSize*0.5;
                    }

	                block.sprite.y = holdMatrix.y+block.y*blockSize  + blockSize - blockSize*0.5;
                    if (game.holdBlock.type == "I") {
                        block.sprite.y -= blockSize*0.5;
                    }
                }
            }

		    requestAnimFrame(animateGame);
		    renderer.render(stage);

            score.setText(game.score);
            level.setText(game.level);
		}

        function attachSpriteToBlock(block) {
            if (block.sprite === undefined) {
                block.sprite = new PIXI.Sprite(blockTexture);
                block.sprite.width = block.sprite.height = blockSize;
                block.sprite.tint = block.color;
                matrix.addChild(block.sprite);
            }
        }


     //Add touch capabilities

     //Create new hammer object
	var hammer = new Hammer.Manager(document.body, {});

	//Add the gestures
	var swipeLeft = new Hammer.Swipe({event: 'swipeleft', direction: 	Hammer.DIRECTION_LEFT});
	var swipeRight = new Hammer.Swipe({event: 'swiperight', direction: Hammer.DIRECTION_RIGHT});
	var swipeUp = new Hammer.Swipe({event: 'swipeup', direction: Hammer.DIRECTION_UP});
	var swipeDown = new Hammer.Swipe({event: 'swipedown', direction: Hammer.DIRECTION_DOWN});
	var pinch = new Hammer.Pinch({event: 'pinch'})
	var singleTap = new Hammer.Tap({event: 'singletap' });

	hammer.add([singleTap, swipeLeft, swipeRight, swipeUp, swipeDown, pinch]);

	//Assign the gestures to a function
	hammer.on('singletap', onTap);
	hammer.on('pinch', onPinch);
	hammer.on("swiperight", onSwipeRight);
	hammer.on("swipeleft", onSwipeLeft);
	hammer.on("swipeup", onSwipeUp);
	hammer.on("swipedown", onSwipeDown);

	//Hard drop
	function onTap(ev)
	{
		game.hardDrop();
	}

	//Move right
	function onSwipeRight(ev)
	{
		game.moveFalling(1, 0);
	}

	//Move Left
	function onSwipeLeft(ev)
	{
		game.moveFalling(-1, 0);
	}

	//Rotate
	function onSwipeUp(ev)
	{
		game.rotate(1);
	}

	//Move down
	function onSwipeDown(ev)
	{
		game.softDrop();
	}
	//Hold
	function onPinch(ev)
	{
		game.hold();
	}




		//On Key Down.... Change the tetrimino
		document.body.onkeydown = function(event) {
			event = event || window.event;
			var keycode = event.charCode || event.keyCode;
			//Hard Drop
			if (keycode === 32) {
				game.hardDrop();
				event.preventDefault();
			}
			//Move Right
			if (keycode === 39 || keycode == 68) {
				game.moveFalling(1, 0);
				event.preventDefault();
			}
			//Move Left
			if (keycode === 37 || keycode == 65) {
				game.moveFalling(-1, 0);
				event.preventDefault();
			}
			//Move Down
			if (keycode === 40 || keycode == 83) {
				game.softDrop();
				event.preventDefault();
			}
			//Hold (shift)
			if (keycode === 16) {
				game.hold();
				event.preventDefault();
			}
			//Rotate CW (up arrow)
			if (keycode === 38 || keycode == 87) {
			    game.rotate(1);
			    event.preventDefault();
			}
			//Rotate CCW (z key)
			if (keycode === 90) {
			    game.rotate(-1);
			    event.preventDefault();
			}
		}

	}
}
